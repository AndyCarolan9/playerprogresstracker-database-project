package playerprogresstracker;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author andyc
 */
public class PlayerProgressTracker {

    /**
     * @param args the command line arguments
     */
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Connection connection = null;
        Statement myStmt = null;
        PreparedStatement pstmt = null;
        ResultSet myRs = null;
        String option = null;

        String dbURL = "jdbc:mysql://localhost:3306/ppt_db";

        do {
            loginMenu(); //  line 517
            System.out.print("Please input your Option: ");
            option = sc.nextLine();
            option = option.toUpperCase();
            switch (option) {
                case "A":
                    signup(myRs, myStmt, connection, pstmt, sc, dbURL); // line 45
                    break;
                case "B":
                    Login(myRs, myStmt, connection, pstmt, sc, dbURL); // line 88
                    break;
            }
        } while (!option.equals("C"));

    }

    public static void signup(ResultSet myRs, Statement myStmt,
            Connection connection, PreparedStatement pstmt, Scanner sc,
            String dbURL) {
        String rootuser = "root";
        String rootpass = "";

        try {
            Class.forName("com.mysql.jdbc.Driver");

            connection = DriverManager.getConnection(dbURL, rootuser, rootpass);

            CreateUser(myRs, myStmt, connection, pstmt, sc); // line 415

            myRs.close();
            myStmt.close();
            connection.close();

        } catch (NullPointerException ne) {
        } catch (SQLException e) {
            for (Throwable t : e) {
                t.printStackTrace();
            }
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (myStmt != null) {
                    myStmt.close();
                }
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
    }

    public static void Login(ResultSet myRs, Statement myStmt,
            Connection connection, PreparedStatement pstmt, Scanner sc,
            String dbURL) {

        String username = "";
        String password = "";

        System.out.print("Please enter your username:");
        username = sc.nextLine();
        System.out.print("Please enter your password:");
        password = sc.nextLine();

        try {
            Class.forName("com.mysql.jdbc.Driver");

            connection = DriverManager.getConnection(dbURL, username, password);

            String option = null;

            do {
                PrintMenu(); // line 480
                System.out.print("Please input your Option: ");
                option = sc.nextLine();
                option = option.toUpperCase();
                switch (option) {
                    case "A":
                        Update(myRs, myStmt, connection, pstmt, sc, username); // line 158
                        break;
                    case "B":
                        Select(myRs, myStmt, connection, pstmt, sc); // line 303
                        break;
                }
            } while (!option.equals("C"));

            myRs.close();
            myStmt.close();
            connection.close();

        } catch (NullPointerException ne) {
        } catch (SQLException e) {
            System.out.println("Username or password incorrect");
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (myStmt != null) {
                    myStmt.close();
                }
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
    }

    /**
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param pstmt
     * @param sc
     */
    public static void Update(ResultSet myRs, Statement myStmt,
            Connection connection, PreparedStatement pstmt, Scanner sc,
            String username) {
        String Option = null;
        do {
            updateMenu(); // line 504
            Option = sc.nextLine();
            Option = Option.toUpperCase();
            switch (Option) {
                case "A":
                    updateAchievement(myRs, myStmt, connection, pstmt,
                            username, "PUBG_Achievements", sc, "PUBG"); //line 200
                    break;
                case "B":
                    updateAchievement(myRs, myStmt, connection, pstmt,
                            username, "RainBowSixSiege_Achievements", sc, "PUBG"); //line 200
                    break;
                case "C":
                    updateAchievement(myRs, myStmt, connection, pstmt,
                            username, "KingdomHeartsII_Achievements", sc, "PUBG"); //line 200
                    break;
                case "D":
                    updateAchievement(myRs, myStmt, connection, pstmt,
                            username, "AssassinsCreed_Achievements", sc, "PUBG"); //line 200
                    break;
            }
        } while (!Option.equals("E"));

    }

    /**
     * creates and executes the achievement update statement
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param pstmt
     * @param username
     * @param table
     * @param sc
     * @param game
     */
    public static void updateAchievement(ResultSet myRs, Statement myStmt,
            Connection connection, PreparedStatement pstmt,
            String username, String table, Scanner sc, String game) {
        String unlocked = "Unlocked";
        String achieve_id = null;
        try {
            achieveSelect(myRs, myStmt, connection, table); //line 385

            System.out.print("Please input the achieve_id of the achievement "
                    + "you would like to update:");
            achieve_id = sc.nextLine();

            System.out.println("Preparing statement");
            String sql;
            sql = "update achievement_player set Status = ?"
                    + " where username = ?"
                    + " and achieve_id = ?";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, unlocked);
            pstmt.setString(2, username);
            pstmt.setString(3, achieve_id);
            pstmt.executeUpdate();
            System.out.println("Record updated");

            System.out.println("");

            userAchieveSelect(myRs, myStmt, connection, username, game); // line 271

        } catch (SQLException se) {
            System.out.println("Database not found.");
            se.printStackTrace();
        }
    }

    /**
     * prints out the achieve id and achieve description
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param game
     */
    public static void achieve_idSelect(ResultSet myRs, Statement myStmt,
            Connection connection, String game) {
        try {
            myStmt = connection.createStatement();

            myRs = myStmt.executeQuery("select achieve_id, achieve_name from achievement"
                    + "where game_id like '%" + game + "%'");

            while (myRs.next()) {
                System.out.println("achievement id: "
                        + myRs.getString("achieve_id")
                        + ", achievement name: "
                        + myRs.getString("achieve_name"));
            }
        } catch (SQLException se) {
            System.out.println("Database not found.");
            se.printStackTrace();
        }
    }

    /**
     * prints out the updated user achievements for a specific game
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param username
     * @param game
     */
    public static void userAchieveSelect(ResultSet myRs, Statement myStmt,
            Connection connection, String username, String game) {
        try {
            myStmt = connection.createStatement();

            myRs = myStmt.executeQuery("Select * from achievement_player "
                    + "where username like '%" + username + "%'"
                    + " and achieve_id like '%" + game + "%'");

            while (myRs.next()) {
                System.out.println("username: "
                        + myRs.getString("username")
                        + ", achieve_id: "
                        + myRs.getString("achieve_id")
                        + ", Status: "
                        + myRs.getString("Status"));
            }
        } catch (SQLException se) {
            System.out.println("Database not found.");
        }
    }

    /**
     * Displays the select menu and runs the games prepared select and achieve
     * select
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param pstmt
     * @param sc
     */
    public static void Select(ResultSet myRs, Statement myStmt,
            Connection connection, PreparedStatement pstmt, Scanner sc) {
        String Soption = null;
        do {
            SelectMenu(); //line 489
            System.out.print("Please input your Option: ");
            Soption = sc.nextLine();
            Soption = Soption.toUpperCase();
            switch (Soption) {
                case "A":
                    gamespreparedSelect(myRs, myStmt, connection, pstmt, "ubisoft_games"); // line 348
                    break;
                case "B":
                    gamespreparedSelect(myRs, myStmt, connection, pstmt, "pubgcorp_games"); // line 348
                    break;
                case "C":
                    gamespreparedSelect(myRs, myStmt, connection, pstmt, "squareenix_games"); // line 348
                    break;
                case "D":
                    achieveSelect(myRs, myStmt, connection, "rainbowsixsiege_achievements"); // line 385
                    break;
                case "E":
                    achieveSelect(myRs, myStmt, connection, "kingdomheartsii_achievements"); // line 385
                    break;
                case "F":
                    achieveSelect(myRs, myStmt, connection, "assassinscreed_achievements"); // line 385
                    break;
                case "G":
                    achieveSelect(myRs, myStmt, connection, "pubg_achievements"); // line 385
                    break;
            }
        } while (!Soption.equals("H"));

    }

    /**
     * calls a view which prints the game name, genre and developer name of
     * specific game
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param pstmt
     * @param table
     */
    public static void gamespreparedSelect(ResultSet myRs, Statement myStmt,
            Connection connection, PreparedStatement pstmt, String table) {
        try {
            myStmt = connection.createStatement();

            String sql;
            sql = "Select * from " + table;

            pstmt = connection.prepareStatement(sql);

            myRs = pstmt.executeQuery();

            while (myRs.next()) {
                String game_name = myRs.getString("game_name");
                String genre = myRs.getString("genre");
                String dev_name = myRs.getString("dev_name");

                System.out.println("Game name: " + game_name);
                System.out.println("Genre: " + genre);
                System.out.println("developer name: " + dev_name);
                System.out.println();
            }

        } catch (SQLException e) {
            System.out.println("Database not found.");
        }
    }

    /**
     * calls a view which selects the achievement name and description of a
     * specific game
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param table
     */
    public static void achieveSelect(ResultSet myRs, Statement myStmt,
            Connection connection, String table) {
        try {
            myStmt = connection.createStatement();

            myRs = myStmt.executeQuery("Select * from " + table);

            while (myRs.next()) {
                System.out.println("Achievement id: "
                        + myRs.getString("achieve_id")
                        + "Achievement Name: "
                        + myRs.getString("achieve_name")
                        + ": achievement description: "
                        + myRs.getString("achieve_desc"));
            }
        } catch (SQLException e) {
            System.out.println("Database not found.");
            e.printStackTrace();
        }
    }

    /**
     * Allows users to create accounts on the database
     *
     * @param myRs
     * @param myStmt
     * @param connection
     * @param pstmt
     * @param sc
     */
    public static void CreateUser(ResultSet myRs, Statement myStmt,
            Connection connection, PreparedStatement pstmt, Scanner sc) {
        String username = null;
        String realname = null;
        String email = null;
        String password = null;

        System.out.print("Please input your username: ");
        username = sc.nextLine();
        System.out.print("Please input your real name: ");
        realname = sc.nextLine();
        System.out.print("Please input your email: ");
        email = sc.nextLine();
        System.out.print("Please input a password: ");
        password = sc.nextLine();

        try {
            myStmt = connection.createStatement();
            String query = "INSERT into player (username, realname, email)"
                    + " values(?,?,?)";

            pstmt = connection.prepareStatement(query);
            pstmt.setString(1, username);
            pstmt.setString(2, realname);
            pstmt.setString(3, email);
            pstmt.execute();

            creategrantuser(myRs, connection, pstmt, username, password); // line 460

            myStmt = connection.createStatement();
            myRs = myStmt.executeQuery("Select * from player where username like '%" + username + "%'");

            while (myRs.next()) {
                System.out.println("username: "
                        + myRs.getString("username") + "\n"
                        + "real name: "
                        + myRs.getString("realname") + "\n"
                        + "email: "
                        + myRs.getString("email"));
            }
        } catch (SQLException e) {
            System.out.println("Database not found or " + username + "already exists");
        }
    }

    public static void creategrantuser(ResultSet myRs, Connection connection,
            PreparedStatement pstmt, String username, String password) {
        try {
            String create = "Create user '" + username + "'@'localhost' Identified by '"
                    + password + "'";
            pstmt = connection.prepareStatement(create);
            pstmt.execute();

            String grant = "GRANT SELECT, INSERT, UPDATE, EXECUTE ON ppt_db.* TO '" + username + "'@'localhost'";
            pstmt = connection.prepareCall(grant);
            pstmt.execute();
            System.out.println("User created!!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * prints the main menu of the application
     */
    public static void PrintMenu() {
        System.out.println("A: Update info");
        System.out.println("B: view games and achievements");
        System.out.println("C: Logout");
    }

    /**
     * print the menu of the select method
     */
    public static void SelectMenu() {
        System.out.println("A: Ubisoft games");
        System.out.println("B: PUBG Corp games");
        System.out.println("C: Square enix games");
        System.out.println("D: Rainbow six siege achievements");
        System.out.println("E: Kingdom Hearts II achievements");
        System.out.println("F: Assassin's Creed achievements");
        System.out.println("G: PUBG achievements");
        System.out.println("H: Exit");

    }

    /**
     * prints the update menu
     */
    public static void updateMenu() {
        System.out.println("A: PUBG achievements");
        System.out.println("B: Rainbow six siege achievements");
        System.out.println("C: Kingdom Hearts II achievements");
        System.out.println("D: Assassin's Creed achievements");
        System.out.println("E: Exit update menu");
        System.out.print("Please input your option:");
    }

    /**
     * prints the login menu (first to appear in application)
     */
    public static void loginMenu() {
        System.out.println("A: Sign up");
        System.out.println("B: Login");
        System.out.println("C: Quit application");
    }
}
